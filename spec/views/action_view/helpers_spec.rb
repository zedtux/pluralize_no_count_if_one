require 'spec_helper'

describe PluralizeNoCountIfOne::ActionView::Helpers do
  describe "#pluralize" do
    context "passing a count of 0" do
      context "without the :no_count_if_one option" do
        it "should return \"0 people\"" do
          pluralize(0, "person").should == "0 people"
        end
      end
      context "with the :no_count_if_one option to false" do
        it "should return \"0 people\"" do
          pluralize(0, "person", nil, no_count_if_one: false).should == "0 people"
        end
      end
      context "with the :no_count_if_one option to true" do
        it "should return \"0 person\"" do
          pluralize(0, "person", nil, no_count_if_one: true).should == "0 people"
        end
      end
    end
    context "passing a count of 1" do
      context "without the :no_count_if_one option" do
        it "should return \"1 person\"" do
          pluralize(1, "person").should == "1 person"
        end
      end
      context "with the :no_count_if_one option to false" do
        it "should return \"1 person\"" do
          pluralize(1, "person", nil, no_count_if_one: false).should == "1 person"
        end
      end
      context "with the :no_count_if_one option to true" do
        it "should return \"person\"" do
          pluralize(1, "person", nil, no_count_if_one: true).should == "person"
        end
      end
    end
    context "passing a count of 2" do
      context "without the :no_count_if_one option" do
        it "should return \"2 people\"" do
          pluralize(2, "person").should == "2 people"
        end
      end
      context "with the :no_count_if_one option to false" do
        it "should return \"2 people\"" do
          pluralize(2, "person", nil, no_count_if_one: false).should == "2 people"
        end
      end
      context "with the :no_count_if_one option to true" do
        it "should return \"2 people\"" do
          pluralize(2, "person", nil, no_count_if_one: true).should == "2 people"
        end
      end
    end
  end
end
