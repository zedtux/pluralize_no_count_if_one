# encoding: utf-8
require "simplecov"
SimpleCov.start

ROOT_PATH = File.join(File.dirname(__FILE__), '..')
$:.unshift ROOT_PATH unless $:.include? ROOT_PATH

require "rubygems"
require "rspec"
require "rails"

require "lib/pluralize_no_count_if_one"

RSpec.configure do |config|
  config.include PluralizeNoCountIfOne::ActionView::Helpers
end
