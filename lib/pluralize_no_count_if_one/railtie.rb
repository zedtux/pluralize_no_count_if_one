module PluralizeNoCountIfOne
  class Railtie < ::Rails::Railtie
    initializer "pluralize_no_count_if_one.configure_view_controller" do |app|
      ActiveSupport.on_load :action_view do
        include PluralizeNoCountIfOne::ActionView::Helpers
      end
    end
  end
end
