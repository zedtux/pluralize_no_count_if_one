module PluralizeNoCountIfOne
  module ActionView
    module Helpers
      # Attempts to pluralize the +singular+ word unless +count+ is 1. If
      # +plural+ is supplied, it will use that when count is > 1, otherwise
      # it will use the Inflector to determine the plural form.
      #
      # pluralize(1, 'person')
      # # => 1 person
      #
      # pluralize(2, 'person')
      # # => 2 people
      #
      # pluralize(3, 'person', 'users')
      # # => 3 users
      #
      # pluralize(0, 'person')
      # # => 0 people
      #
      # pluralize(0, 'person', nil, no_count_if_one: true)
      # # => 0 people
      #
      # pluralize(1, 'person', nil, no_count_if_one: true)
      # # => person
      def pluralize(count, singular, plural = nil, options={no_count_if_one: false})
        word = if (count == 1 || count =~ /^1(\.0+)?$/)
          singular
        else
          plural || singular.pluralize
        end

        count ||= 0
        count = nil if options[:no_count_if_one] && count == 1

        "#{count} #{word}".strip
      end
    end
  end
end
