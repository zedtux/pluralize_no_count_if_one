# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'pluralize_no_count_if_one/version'

Gem::Specification.new do |spec|
  spec.name          = "pluralize_no_count_if_one"
  spec.version       = PluralizeNoCountIfOne::VERSION
  spec.authors       = ["zedtux"]
  spec.email         = ["zedtux@zedroot.org"]
  spec.description   = %q{Add the missing :no_count_if_one option to the Rails pluralize helper}
  spec.summary       = %q{In the case you set the new option :no_count_if_one to true and the collection has only one value it will not show the count.}
  spec.homepage      = "https://github.com/zedtux/pluralize_no_count_if_one"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency "railties"

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "simplecov"
  spec.add_development_dependency "rails"
end
