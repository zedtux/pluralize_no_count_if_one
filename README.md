# PluralizeNoCountIfOne

[![Build Status](https://travis-ci.org/zedtux/pluralize_no_count_if_one.png?branch=master)](https://travis-ci.org/zedtux/pluralize_no_count_if_one)

Add the missing `:no_count_if_one` option to the Rails [pluralize helper](http://api.rubyonrails.org/classes/ActionView/Helpers/TextHelper.html#method-i-pluralize)

In the case you set the new option `:no_count_if_one` to true and the collection has only one value it will not show the count.

## Installation

Add this line to your application's Gemfile:

    gem 'pluralize_no_count_if_one'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install pluralize_no_count_if_one

## Usage

    = pluralize(0, "dependency", nil, no_count_if_one: true)
    #=> 0 dependencies
    = pluralize(1, "dependency", nil, no_count_if_one: true)
    #=> dependency
    = pluralize(1, "dependency", nil, no_count_if_one: false)
    #=> 1 dependency
    = pluralize(15, "dependency", nil, no_count_if_one: true)
    #=> 15 dependencies

I'm using this gem for my [brewformulas.org](https://github.com/zedtux/brewformulas.org) project in order to show the following sentence:

 * In the case the formula has only one dependency:
 > The following dependency will be installed if you install Abcl:

 * In the case the formula has multiple dependencies:
 > The following 7 dependencies will be installed if you install Abcde:

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
